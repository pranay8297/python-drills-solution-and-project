#p1
import numpy as np
import pandas as pd
import sys


a = np.array([2,2], dtype = float)
print(a.dtype)
a = np.array([2**63 -1  , 2**63 - 1])
print(a)
print(a.sum())
print(a.dtype)
print(a + 1)
print(a.dtype)
print(a)
print(a.sum())
b = np.array([2,3] ,dtype = int)
print(b)
print(b+1)
#p2
print(7/2)
print(int(7/2))
print(7//2)
print(-7/2)
print(-7//2)

#p3 strings

x = "one two three four."

print(x[-3:])
print(x[:10])
print(x[4:10])
print(len(x))
l = len(x)
y = x.split()
print(y)
def str_split(x):
    temp = ""
    arr = []
    for i in x:
   
        if (i is ' ') or x.endswith(i+'.'):
            temp = temp + i
            arr.append(temp)
            temp = ''
        else:
            temp = temp + i
    return arr
print(str_split(x))

y = x.upper()
print(y)
z = x.capitalize()
print(z)

#capitalizing the first letter of the string
cap_x = ''
y = x.split()
for i in y:
    cap_x = cap_x + i.capitalize()
    cap_x += ' '
print(cap_x)

#converting x into uppercase

z = x.upper()
print(z)
    
#lists
#p1

x = [3, 1, 2, 4, 1, 2]

s = sum(x)
print(s)

#p2
print(len(x))

#p3
print(x[-3:])

#p4
print(x[:3])
 #p5

y = sorted(x)
print(y)

#p6 and p7
x.append(10)
x.append(11)
x.pop()
print(x)


'''

x = [3, 1, 2, 4, 1, 2]
y = [0, 1, 2, 3, 4, 5]
print(x[:3])
x = x[:3] + x[4:]
print(x)
print(x[3:4])
x.pop(3)
print(x)

'''
c = 0
for i in x:
    if i == 1:
        c += 1
print(c)

if 10 in x:
    print("true")
else: print("false")

y = [5, 1, 2, 3]
for i in y:
    x.append(i)


z = y
y.append(44)
print(y)
print(z)
import copy
z = copy.deepcopy(y)
y.append(55)
print(y)
print(z)

#Can a list contain elements of different data-types?
#yes


#Which data structure does Python use to implement a list?
#dqueue


# y = 2

#for range and while

x = [1, 2, 3, 4]
#p1
for i in x:
    print(i)

for i in x:
    print(i, end= ' ')

#Print all the elements and their indexes using the enumerate function
ex = enumerate(x , start = 0)
print(ex)

print(list(ex))

for i in range(10 , -1 , -1):
    print(i)

#while loop
#print numbers in descending order
x = 10
while x >= 0:
    print(x)
    x -= 1

#prime numbers before 100 
j = 2
while True:
    if j < 100:
        c = 0
        for i in range(2,j):
            if j%i == 0:
                c += 1
        if c == 0:
            print(j)
        else:
            pass
        j += 1
    else:
        break
    
    
    
#function to return all the primes before a number n
def prime_nums(n):
    primes = []
    for i in range(2 , n):
        c = 0
        for j in range(2 , i):
            if i%j == 0:
                c += 1
        if c == 0:
            primes.append(i)
    return primes

print(prime_nums(100))

#helper function
def is_prime(x):
    c = 0
    for i in range(2 ,x):
        if x%i == 0:
            c += 1
    if c != 0:
        return False
    else:
        return True
print(is_prime(97))
                    

#dicts
#string_repetitions
def word_counts(string):
    arr = string.split()
    d = {}
    for i in arr:
        if i in d:
            d[i] += 1
        else:
            d[i] = 1
    return d

string = 'Python is an interpreted, high-level, general-purpose programming language. Python interpreters are available for many operating systems. Python is a multi-paradigm programming language. Object-oriented programming and structured programming are fully supported.'
print(word_counts(string))  


#printing the dict
def print_dict(dict):
    for i in dict:
        print(i + ' : '+ str(dict[i]))
print_dict(word_counts(string))


#classes
import math

class Point:
    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def distance(self, obj):
        dist = math.sqrt(((obj.x ** 2) - (self.x ** 2)) + ((obj.y ** 2) - (self.y ** 2)))
        return dist

p1 = Point(3 ,4)
p2 = Point(5, 10)
print(p1.distance(p2))    



#create a csv file
def numbers_and_squares(n , file_name):
    file = open(file_name , "a")
    for i in range(1, n+1):
        file.write('{} , {} \n'.format(i , i**2))
    file.close()
       
numbers_and_squares(4 , 'xyz.csv')

#sets 

file = open('sample.txt' , 'w')
file.write('indiana jones is a archeologist and he does some adventurous stuff and they are entertaining. finally indian jones is great')
file.close()
def command(file_name):
    file = open(file_name , 'r')
    x = file.read()
    file.close()
    print(sorted(list(set(x.split()))))


command('sample.txt')


























































































































































































































































































































































































































































 


















