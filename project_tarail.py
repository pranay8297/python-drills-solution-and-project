import numpy as np
import pandas as pd
import sys
import random
import copy

def split(x):
    z = x
    n = []
    while z > 0:
        n.append(z%10)
        z = z//10
    n.reverse()
    return n

x = random.randrange(10000, 100000)

xn = split(x)
for i in range(10):
    y = int(input('enter a number to play : '))
    txn = copy.deepcopy(xn)
    yn = split(y)
    cows = 0
    bulls  = 0
    for j in yn:
        if j in txn:
            txn.remove(j)
            cows += 1
    l = len(xn)
    for k in range(l):
        if xn[k] == yn[k]:
            bulls += 1
    if bulls == 5:
        print('bulls eye! you won the game')
        break
        
    print(bulls)
    print(cows)

print(sum(xn))
print(sum(yn))


