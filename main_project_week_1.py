import random
import copy

class MasterMind:
    def __init__(self):
        self.x = random.randrange(10000, 100000)
        self.y = None
    
    def split(self, y):
        self.z = y
        self.n = []
        while self.z > 0:
            self.n.append(self.z%10)
            self.z = self.z//10
            self.n.reverse()
        return self.n

    def guess(self):
        self.xn = self.split(self.x)
        print(self.x)
        for i in range(10):
            self.y = int(input('guess a 5 digit number : '))
            self.yn = self.split(self.y)
            self.txn = copy.deepcopy(self.xn)
            self.cows = 0
            self.bulls = 0
            for j in self.yn:
                if j in self.txn:
                    self.cows += 1
                    self.txn.remove(j)
            self.l = len(self.xn)
            for k in range(self.l):
                if self.xn[k] == self.yn[k]:
                    self.bulls = self.bulls + 1
            if self.bulls == self.l:
                print("bulls eye! you made it... the number is "+ str(self.y))
                break
            
            if self.xn[0] == self.yn[0]:
                print('very near')
            else:
                print('think smart')
            
            print('cows : '+str(self.cows - self.bulls))
            print('bulls : '+str(self.bulls))
            
            if (self.bulls < 5) and (i == 4):
                self.reset()
        
    def reset(self):
        print('poor, try again once')
            
game = MasterMind()
game.guess()

y = [1,1,1,3,4]
x = [1,2,3,4,5]

            
        